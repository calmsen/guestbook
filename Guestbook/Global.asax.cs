﻿using Guestbook.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Guestbook
{
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Строка подключения к бд. 
        /// </summary>
        private static string _connectionString;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // инициалищируем базу данных
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultDatabase"].ConnectionString;
            DefaultDatabase db = new DefaultDatabase(_connectionString);
            db.Init();
        }
        /// <summary>
        /// Запускается перед выполнением текущего запроса
        /// </summary>
        protected void Application_BeginRequest()
        {
            // создадим контекст базы данных и вложим его в HttpContext
            DefaultDatabase db = new DefaultDatabase(_connectionString);
            db.OpenConnection();
            HttpContext.Current.Items["DefaultDatabase"] = db;
        }
        /// <summary>
        /// Запускается после выполнения текущего запроса
        /// </summary>
        protected void Application_EndRequest()
        {
            // закроем соединение с бд
            if (HttpContext.Current.Items["DefaultDatabase"] != null)
                ((DefaultDatabase)HttpContext.Current.Items["DefaultDatabase"]).CloseConnection();
        }
    }
}
