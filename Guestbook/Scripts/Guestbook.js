﻿/*
 * Класс для работы с гостевой книгой
 */
var Guestbook = (function ($, window, undefined) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Guestbook() {
        this.init.apply(this, arguments);
    }
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию.
     */
    Guestbook.prototype = {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
        }
        /**
         * Инициализирует экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this, this.defaults, options);

            this.createMessageBtn = $("#createMessageBtn");
            this.clearMessageBtn = $("#clearMessageBtn");
            this.createMessageForm = $("#createMessageForm");
            
        }
        /*
         * Устанавливает события на форму добавления сообщения в гостевую книгу.
         */
        , setCreateMessageFormEvents: function () {
            var gbContext = this;
            $(function () {
                gbContext.createMessageBtn.on("click", $.proxy(gbContext.createMessageBtnOnClick, gbContext));
                gbContext.clearMessageBtn.on("click", $.proxy(gbContext.clearMessageBtnOnClick, gbContext));
            });
        }
        /**
         * Обработчик события click на кнопке "Добавить"
         * @param {JQueryEventObject} event
         */
        , createMessageBtnOnClick: function (event) {
            event.preventDefault();
            // если форма валидна то создадим сообщение
            var formIsValid = this.createMessageForm.valid();
            if (formIsValid)
            {
                this.createMessage();
            } 
            
        }
        /**
         * Обработчик события click на кнопке "Очистить"
         * @param {JQueryEventObject} event
         */
        , clearMessageBtnOnClick: function (event) {
            this.resetMessageForm();
        }
        /*
         * Создадим сообщение на удаленном сервере
         */
        , createMessage: function () {
            if (this.denyAjax)
                return;
            this.denyAjax = true;
            // отобразим состояние кнопки на loading
            this.createMessageBtn.button("loading");
            // отправим запрос на сервер
            var gbContext = this;
            $.ajax({
                url: "/Home/CreateMessage",
                type: "post",
                data: this.createMessageForm.serialize(),
                success: function (html) {
                    // удалим форму, отобразим список сообщений
                    $("#guestbookBody").html(html);
                    // изменим заголовок документа и url в поле адреса браузера
                    window.document.title = "Гостевая книга";
                    window.history.replaceState({}, "", "/");
                },
                error: function (xhr, state) {
                    // получим текст ошибки в зависимости от статуса ответа
                    var errorText;
                    if (xhr.status == 400)
                        errorText = "Неправильно заполнены поля.";
                    else 
                        errorText = "Произошла ошибка при сохранении. Возможно были введены опасные символы.";
                    // удалим старое сообщение об ошибке и добавим новое сообщение об ошибке
                    $("#error500").remove();
                    $("<p/>", {
                        text: errorText,
                        id: "error500",
                        "class": "text-danger"
                    }).prependTo(gbContext.createMessageForm)
                    // изменим состояние кнопки на обычное и разрешим отправку нового аякс запроса
                    gbContext.createMessageBtn.button('reset');                                   
                    gbContext.denyAjax = false;
                }
            });
        }
        /*
         * Сбрасывает поля, ошибк и состояния об ошибке
         */
        , resetMessageForm: function () {
            this.createMessageForm[0].reset();
            // сбросим состояния внутри плагина jQuery Validate
            this.createMessageForm.validate().resetForm();

            // сбросим состояния unobtrusive validation
            this.createMessageForm.find("[data-valmsg-summary=true]")
                .removeClass("validation-summary-errors")
                .addClass("validation-summary-valid")
                .find("ul").empty();                     
            this.createMessageForm.find("[data-valmsg-replace]")
                .removeClass("field-validation-error")
                .addClass("field-validation-valid")
                .empty();
        }
    };
    return Guestbook;
}(jQuery, window, undefined));