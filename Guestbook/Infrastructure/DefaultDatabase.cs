﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Guestbook.Infrastructure
{
    /// <summary>
    /// Класс представляет databasecontext для работы с базой данной.
    /// </summary>
    public class DefaultDatabase
    {
        private SqlConnection conn;
        private SqlTransaction transaction;
        /// <summary>
        /// Инициазирует экземпляр класса DefaultDatabase
        /// </summary>
        /// <param name="connectionString">Строка подключения к бд</param>
        public DefaultDatabase(string connectionString)
        {
            conn = new SqlConnection(connectionString);
        }
        /// <summary>
        /// Открывает соединение
        /// </summary>
        public void OpenConnection()
        {
            conn.Open();
        }
        /// <summary>
        /// Закрывает соединение
        /// </summary>
        public void CloseConnection()
        {
            if (conn != null)
                conn.Close();
        }
        /// <summary>
        /// Создает команду для выполнения запроса
        /// </summary>
        /// <param name="cmdText">sql запрос</param>
        /// <returns></returns>
        public SqlCommand CreateCommand(string cmdText)
        {
            SqlCommand cmd = new SqlCommand(cmdText, conn);
            if (transaction != null)
                cmd.Transaction = transaction;
            return cmd;
        }
        /// <summary>
        /// Начинает транзакцию
        /// </summary>
        public void BeginTransaction()
        {
            transaction = conn.BeginTransaction();
        }
        /// <summary>
        /// Фиксация транзакции
        /// </summary>
        public void CommitTransaction()
        {
            transaction.Commit();
            transaction = null;
        }
        /// <summary>
        /// Откат транзакции
        /// </summary>
        public void RollbackTransaction()
        {
            transaction.Rollback();
            transaction = null;
        }
        /// <summary>
        /// Инициализирует базу данных. Создает необходимые таблицы и наполняет динамический контент
        /// </summary>
        public void Init() 
        {
            
            try
            {
                OpenConnection();

                // создаем таблицу Users, если еще не создавали
                SqlCommand cmd = CreateCommand(
                @"if not exists (select * from sysobjects where name='Users' and xtype='U')
                create table Users (
                    ID int not null identity(1,1) primary key, 
                    Name nvarchar(64) not null,
                    Email nvarchar(64) not null,
                    Homepage nvarchar(512) null
                )");

                cmd.ExecuteNonQuery();

                // создаем таблицу Messages, если еще не создавали
                cmd = CreateCommand(
                    @"if not exists (select * from sysobjects where name='Messages' and xtype='U')
                    create table Messages (
                        ID int not null identity(1,1) primary key,
                        Text nvarchar(4000) not null,
                        CreatedDate datetime not null,
                        UserID int foreign key references Users(ID)
                    )");


                cmd.ExecuteNonQuery();

                // создаем таблицу ClientInfos, если еще не создавали
                cmd = CreateCommand(
                    @"if not exists (select * from sysobjects where name='ClientInfos' and xtype='U')
                    create table ClientInfos (
                        MessageID int not null primary key foreign key references Messages(ID),
                        IP varchar(64) not null,
                        BrowserType varchar(512) not null,
                        BrowserVersion varchar(64) not null
                    )");
                cmd.ExecuteNonQuery();
                // узнаем генерировали ли записи в бд, если генереровали то завершим метод
                cmd = CreateCommand("select count(*) from Messages");

                int count = (int)cmd.ExecuteScalar();
                if (count > 0)
                    return;
                
                // добавим пользователей 
                string[] names = {
                    "andrey",
                    "ivan",
                    "vladimir",
                    "alexander"
                };
                int[] userIDs = new int[names.Length];
                for (int i = 0; i < names.Length; i++)
                {
                    cmd = CreateCommand(
                        @"insert into Users (Name, Email, Homepage) values (@Name, @Email, @Homepage)
                        select ID from Users where @@rowcount > 0 and ID = scope_identity()"
                    );
                    cmd.Parameters.Add(new SqlParameter("@Name", names[i]));
                    cmd.Parameters.Add(new SqlParameter("@Email", names[i] + "@gmail.com"));
                    cmd.Parameters.Add(new SqlParameter("@Homepage", "http://" + names[i] + ".com/"));
                    userIDs[i] = (int)cmd.ExecuteScalar();
                } 
                // добавим случайные сообщения
                string[] text = {
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                    "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                    "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                    "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                };
                for (int i = 0; i < 500; i++)
                {
                    int m = new Random().Next(0, text.Length - 1);
                    int u = new Random().Next(0, userIDs.Length - 1);

                    cmd = CreateCommand(
                        @"insert into Messages (Text, CreatedDate, UserID) values (@Text, getdate(), @UserID)
                        select ID from Messages where @@rowcount > 0 and ID = scope_identity()"
                    );

                    cmd.Parameters.Add(new SqlParameter("@Text", text[m]));
                    cmd.Parameters.Add(new SqlParameter("@UserID", userIDs[u]));
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                CloseConnection();
            }
        }
    }
}