﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Guestbook.Models
{
    /// <summary>
    /// Данный класс представляет ссылки для списка, по которым будет идти сортировка
    /// </summary>
    public class HeaderLink
    {
        /// <summary>
        /// Название ссылки
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Активная ли ссылка
        /// </summary>
        public SortDirectionEnum? ActiveDirection { get; set; }
        /// <summary>
        /// Параметры ссылки
        /// </summary>
        public RouteValueDictionary RouteValues { get; set; }
    }
}