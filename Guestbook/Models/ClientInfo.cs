﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Guestbook.Models
{
    /// <summary>
    /// Данный класс представляет информацию о клиенте пользователя
    /// </summary>
    public class ClientInfo
    {
        /// <summary>
        /// Идентификатор сообщения гостевой книги в бд. Так же основной ключ записи ClientInfo
        /// </summary>
        public int MessageID { get; set; }
        /// <summary>
        /// IP адрес 
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// Название браузера пользователя
        /// </summary>
        public string BrowserType { get; set; }
        /// <summary>
        /// Версия браузера
        /// </summary>
        public string BrowserVersion { get; set; }
    }
}