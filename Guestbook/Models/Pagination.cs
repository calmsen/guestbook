﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Guestbook.Models
{

    /// <summary>
    /// Класс содержит свойства и методы для работы с пагинацией
    /// </summary>
    public class Pagination
    {
        /// <summary>
        /// Номер текущей страницы
        /// </summary>
        private int _currentPage = 1;
        public int CurrentPage { get { return _currentPage; } }
        /// <summary>
        /// Номер последней страницы
        /// </summary>
        private int _lastPage = 1;
        public int LastPage { get { return _lastPage; } }
        /// <summary>
        /// Номер предыдущей страницы относительно текущей
        /// </summary>
        private int _prevPage = 1;
        public int PrevPage { get { return _prevPage; } }
        /// <summary>
        /// Номер следующей страницы относительно текущей 
        /// </summary>
        private int _nextPage = 1;
        public int NextPage { get { return _nextPage; } }   
        /// <summary>
        /// Сколько записей показывать на странице
        /// </summary>
        private int _pageItemsAmount = 10;
        public int PageItemsAmount { get { return _pageItemsAmount; } }  
        /// <summary>
        /// Всего записей в бд
        /// </summary>
        private int _itemsAmount = 0;
        public int ItemsAmount { get { return _itemsAmount; } } 
        /// <summary>
        /// Количествл записей на текущей странице 
        /// </summary>
        private int _currentPageItemsAmount = 0;
        public int CurrentPageItemsAmount { get { return _currentPageItemsAmount; } }      
        /// <summary>
        /// Количество записей которые нужно пропустить при выборке. (_currentPage - 1) * _pageItemsAmount
        /// </summary>
        private int _offset = 0;
        public int Offset { get { return _offset; } }
        /// <summary>
        /// Пункты пагинации
        /// </summary>
        private List<Page> _pages;
        public List<Page> Pages { get { return _pages; } }
        /// <summary>
        /// Значения для формирования ссылок пунктов пагинации
        /// </summary>
        private Dictionary<string, object> _routeParams;   
        /// <summary>
        /// Инициазирует экземпляр класса Pagination
        /// </summary>
        /// <param name="currentPage">Текущая страница</param>
        /// <param name="pageItemsAmount">Количество записей на странице</param>
        /// <param name="itemsAmount">Всего записей</param>
        /// <param name="routeParams">Параметры для ссылок</param>
        public Pagination(int currentPage, int pageItemsAmount, int itemsAmount, Dictionary<string, object>  routeParams)
        {
            _currentPage = currentPage;
            _pageItemsAmount = pageItemsAmount;
            _itemsAmount = itemsAmount;
            _routeParams = routeParams;
            _pages = new List<Page>();

            // устанавливаем значения по умолчанию, еслм это будет необходимо
            SetDefaultValues();
            // Вычисляет зависимые значения
            CalculateValues();
            // создаем пункты пагинации
            CreatePages();
        }
        /// <summary>
        /// Устанавливает значения по умолчанию, еслм значения неправильные
        /// </summary>
        private void SetDefaultValues()
        {
            if (_currentPage <= 0)
                _currentPage = 1;

            if (_pageItemsAmount <= 0)
                _pageItemsAmount = 10;

            if (_itemsAmount < 0)
                _itemsAmount = 0;
        }
        /// <summary>
        /// Вычисляет зависимые значения
        /// </summary>
        private void CalculateValues()
        {
            _offset = (_currentPage - 1) * _pageItemsAmount;

            if (_itemsAmount > 0 && _itemsAmount > _pageItemsAmount)
                _lastPage = (int)Math.Ceiling((double)_itemsAmount / _pageItemsAmount);
            if (_lastPage <= 0)
                _lastPage = 1;

            if (_currentPage < _lastPage)
                _currentPageItemsAmount = _pageItemsAmount;
            else
                _currentPageItemsAmount = _itemsAmount - (_lastPage - 1) * _pageItemsAmount;

            _prevPage = _currentPage - 1;
            if (_prevPage <= 0)
                _prevPage = 1;

            _nextPage = _currentPage + 1;
            if (_nextPage > _lastPage)
                _nextPage = _lastPage;
        }
        /// <summary>
        /// Создает пункты пагинации
        /// </summary>
        private void CreatePages()
        {
            int limit = 9;
            int middle = 5;
            _pages.Clear();
            bool leftDotPage = false;
            bool rightDotPage = false;
            if (_lastPage > limit)
                rightDotPage = true;
            // если страниц меньше чем limit(вкл) или текущая страница меньше middle(вкл), то покажем все начальные страницы
            if (_lastPage <= limit)
            {
                for (int i = 1; i <= _lastPage; i++)
                    _pages.Add(new Page
                    {
                        Value = i,
                        Caption = i.ToString(),
                        IsCurrenPage = i == _currentPage,
                        RouteValues = CreateRouteValues(i, _routeParams)
                    });
            }
            else if (_currentPage <= middle)
            {
                for (int i = 1; i <= limit; i++)
                    _pages.Add(new Page
                    {
                        Value = i,
                        Caption = i.ToString(),
                        IsCurrenPage = i == _currentPage,
                        RouteValues = CreateRouteValues(i, _routeParams)
                    });
            }
            // иначе сдвигаем страницы на разницу текущей страницы от middle
            else
            {
                leftDotPage = true;
                int n = _lastPage - limit; // количество позиций на которые можно сдвинуть влево 
                int c = _currentPage - middle; // количество позиций относительно текущей страницы
                if (c > n)
                {
                    c = n;
                    rightDotPage = false;
                }

                for (int i = 1 + c, l = c + limit; i <= l; i++)
                    _pages.Add(new Page
                    {
                        Value = i,
                        Caption = i.ToString(),
                        IsCurrenPage = i == _currentPage,
                        RouteValues = CreateRouteValues(i, _routeParams)
                    });
            }
            // нужно показывать первые и последние страницы, то покажем 1, 2, предпоследнюю и последнюю страницы. То есть картина будет такая: 
            //  ___ ___ ____ ___ ___ ___ ___ ____ ____ ____
            // | 1 | 2 | .. | 6 | 7 | 8 | 9 | .. | 14 | 15 |
            //
            for (var i = 0; i < _pages.Count; i++)
            {
                if (i <= 2 && leftDotPage)
                {
                    if (i == 0)
                    {
                        _pages[i].Caption = "1";
                        _pages[i].Value = 1;
                        _pages[i].RouteValues = CreateRouteValues(1, _routeParams);
                    }
                    else if (i == 1)
                    {
                        _pages[i].Caption = "2";
                        _pages[i].Value = 2;
                        _pages[i].RouteValues = CreateRouteValues(2, _routeParams);
                    }
                    else if (i == 2)
                    {
                        _pages[i].Caption = "..";
                    }
                }
                else if (i >= limit - 3 && rightDotPage)
                {
                    if (i == limit - 1)
                    {
                        _pages[i].Caption = _lastPage.ToString();
                        _pages[i].Value = _lastPage;
                        _pages[i].RouteValues = CreateRouteValues(_lastPage, _routeParams);
                    }
                    else if (i == limit - 2)
                    {
                        _pages[i].Caption = (_lastPage - 1).ToString();
                        _pages[i].Value = _lastPage - 1;
                        _pages[i].RouteValues = CreateRouteValues(_lastPage - 1 , _routeParams);
                    }
                    else if (i == limit - 3)
                    {
                        _pages[i].Caption = "..";
                    }
                }
            }

            // добавим предыдущую и следующую странцы. Получится картина такая:
            //  ____ ___ ___ ____ ___ ___ ___ ___ ____ ____ ____ ____
            // | << | 1 | 2 | .. | 6 | 7 | 8 | 9 | .. | 14 | 15 | >> |
            //
            _pages.Insert(0, new Page
            {
                Value = _prevPage,
                Caption = "&laquo;",
                Disabled = _currentPage == 1,
                RouteValues = CreateRouteValues(_prevPage, _routeParams)
            });
            _pages.Add(new Page
            {
                Value = _nextPage,
                Caption = "&raquo;",
                Disabled = _currentPage == _lastPage,
                RouteValues = CreateRouteValues(_nextPage, _routeParams)
            });
        }
        /// <summary>
        /// Создает словарь RouteValueDictionary из других словарей
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="values">Другие параметры</param>
        /// <returns></returns>
        private RouteValueDictionary CreateRouteValues(int page, Dictionary<string, object> values)
        {
            RouteValueDictionary routeValues = new RouteValueDictionary();
            if (values != null && values.Count > 0)
                values.ToList().ForEach(x => { routeValues[x.Key] = x.Value; });
            routeValues.Add("page", page);
            return routeValues;
        }
    }

    /// <summary>
    /// Класс содержит данные об одном пункте пагинации
    /// </summary>
    public class Page
    {
        /// <summary>
        /// Номер страницы
        /// </summary>
        public int Value { get; set; }
        /// <summary>
        /// Отображаемое значение. Актуально для PrevPage и NextPage
        /// </summary>
        public string Caption { get; set; }
        /// <summary>
        /// Активный или некактивная ссылка. Актуально для PrevPage и NextPage
        /// </summary>
        public bool Disabled { get; set; } 
        /// <summary>
        /// Является ли пункт текущей страницей
        /// </summary>
        public bool IsCurrenPage { get; set; }
        /// <summary>
        /// Значения для формирования ссылки. Совокупность параметров Params (с объекта пагинации) и Value
        /// </summary>
        public RouteValueDictionary RouteValues { get; set; } 
    }
}