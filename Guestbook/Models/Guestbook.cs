﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Guestbook.Models
{
    /// <summary>
    /// Данный класс представляет гостевую книгу
    /// </summary>
    public class GuestbookViewModel
    {
        /// <summary>
        /// Заголовок гостевой книги
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Список сообщений
        /// </summary>
        public List<Message> Messages { get; set; }
        /// <summary>
        /// Пейджинг
        /// </summary>
        public Pagination Pagination { get; set; }
        /// <summary>
        /// Заголовки списка, по которым будет сортировка
        /// </summary>
        public List<HeaderLink> HeaderLinks { get; set; }
    }
}