﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Guestbook.Models
{
    /// <summary>
    /// Данный класс представляет сообщение, оставленное в гостевой книге
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Идентификатор сообщения в бд
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Текст сообщения
        /// </summary>
        [Display(Name = "Введите сообщение")]
        [Required(ErrorMessage = "Требуется ввести сообщение")]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }
        /// <summary>
        /// Дата создания сообщения
        /// </summary>
        public DateTime CreatedDate { get; set; }
        /// <summary>
        /// Идентификатор пользователя в бд
        /// </summary>
        public int UserID { get; set; }
        /// <summary>
        /// Информация о пользователе
        /// </summary>
        public User User { get; set; }
    }
}