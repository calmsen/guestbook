﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Guestbook.Models
{
    /// <summary>
    /// Данный класс представляет данные о пользователе
    /// </summary>
    public class User
    {
        /// <summary>
        /// Идентификатор сообщения в бд
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Имя пользователя
        /// </summary>
        [Display(Name = "Введите свое имя")]
        [Required(ErrorMessage = "Требуется ввести свое имя")]
        public string Name { get; set; }
        /// <summary>
        /// Электронный адрес пользователя
        /// </summary>
        [Display(Name = "Введите свой e-mail")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Требуется ввести свою e-mail")]
        [EmailAddress(ErrorMessage = "Некорректно введен email")]
        public string Email { get; set; }
        /// <summary>
        /// Адрес своего сайта
        /// </summary>
        [Display(Name = "Введите адрес своей страницы")]
        [DataType(DataType.Url)]
        [Url(ErrorMessage = "Некорректно введен адрес страницы")]
        public string Homepage { get; set; }
    }
}