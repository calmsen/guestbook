﻿using Guestbook.Infrastructure;
using Guestbook.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Guestbook.Repositories
{
    /// <summary>
    /// Данный класс представляет методы для работы с сообщениями гостевой книги
    /// </summary>
    public class MessagesRepository
    {
        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private DefaultDatabase _db;
        /// <summary>
        /// Инициазирует экземпляр класса MessagesRepository
        /// </summary>
        /// <param name="db">Контекст базы данных</param>
        public MessagesRepository(DefaultDatabase db)
        {
            _db = db;
        }
        /// <summary>
        /// Создает сообщение в гостевой книге
        /// </summary>
        /// <param name="message">Объект сообщения</param>
        public void CreateMessage(Message message)
        {
            SqlCommand cmd = _db.CreateCommand(
                @"insert into Messages (Text, CreatedDate, UserID) values (@Text, getdate(), @UserID)
                select ID from Messages where @@rowcount > 0 and ID = scope_identity()"
            );

            cmd.Parameters.Add(new SqlParameter("@Text", message.Text));
            cmd.Parameters.Add(new SqlParameter("@UserID", message.UserID));

            message.ID = (int)cmd.ExecuteScalar();
        }
        /// <summary>
        /// Получает список сообщений для текущей страницы, предварительно отсортировав записи
        /// </summary>
        /// <param name="sortType">Тип, по которому надо сортировать</param>
        /// <param name="sortDirection">Направление сортировки</param>
        /// <param name="offset">Количество записей, которые нужно пропустить</param>
        /// <param name="numberOfItems">Число записей, которые надо выбрать</param>
        /// <returns></returns>
        public List<Message> GetMessages(SortTypeEnum sortType, SortDirectionEnum sortDirection, int offset, int numberOfItems)
        {
            List<Message> messages = new List<Message>();

            SqlDataReader rdr = null;
            try
            {
                // построим join выражение c Users если нужно
                string joinWithUsers = string.Empty;
                if (sortType == SortTypeEnum.UserName || sortType == SortTypeEnum.Email)
                    joinWithUsers = " join Users u on u.ID = m.UserID";
                // получим название колонки для выражения order
                string columnNameForOrderExpr = null;          
                switch (sortType)
                {
                    case SortTypeEnum.UserName: columnNameForOrderExpr = "u.Name";
                        break;
                    case SortTypeEnum.Email: columnNameForOrderExpr = "u.Email";
                        break;
                    case SortTypeEnum.CreatedDate: columnNameForOrderExpr = "m.CreatedDate";
                        break;
                }
                // используем обычный string.Format вместо параметризованного запроса
                // так как нет строковых параметров, которые пришли от клиента
                SqlCommand cmd = _db.CreateCommand(string.Format(
                    @"select m.ID, m.Text, m.CreatedDate, m.UserID from Messages m {0} 
                        order by {1} {2} offset {3} rows fetch next {4} rows only",
                    joinWithUsers,
                    columnNameForOrderExpr, 
                    sortDirection, 
                    offset, 
                    numberOfItems
                ));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    messages.Add(new Message
                    {
                        ID = Convert.ToInt32(rdr["ID"]),
                        Text = rdr["Text"].ToString(),
                        CreatedDate = DateTime.Parse(rdr["CreatedDate"].ToString()),
                        UserID = Convert.ToInt32(rdr["UserID"])
                    });
                }
            }
            finally
            {
                if (rdr != null)
                    rdr.Close();
            }
            return messages;
        }
        /// <summary>
        /// Получает количество сообщений в бд
        /// </summary>
        /// <returns></returns>
        public int GetNumberOfItems()
        {
            SqlCommand cmd = _db.CreateCommand("select count(*) from Messages");
 
            return (int)cmd.ExecuteScalar();
        }
    }
}