﻿using Guestbook.Infrastructure;
using Guestbook.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Guestbook.Repositories
{
    /// <summary>
    /// Данный класс представляет методы для работы с информацией по клиенту
    /// </summary>
    public class ClientInfosRepository
    {
        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private DefaultDatabase _db;
        /// <summary>
        /// Инициазирует экземпляр класса ClientInfosRepository
        /// </summary>
        /// <param name="db">Контекст базы данных</param>
        public ClientInfosRepository(DefaultDatabase db)
        {
            _db = db;
        }
        /// <summary>
        /// Создает информациею о клиенте
        /// </summary>
        /// <param name="clientInfo">Объект содержащий информацию о клиенте</param>
        public void CreateClientInfo(ClientInfo clientInfo)
        {
            SqlCommand cmd = _db.CreateCommand(
                @"insert into ClientInfos (MessageID, IP, BrowserType, BrowserVersion) values 
                    (@MessageID, @IP, @BrowserType, @BrowserVersion)"
            );

            cmd.Parameters.Add(new SqlParameter("@MessageID", clientInfo.MessageID));
            cmd.Parameters.Add(new SqlParameter("@IP", clientInfo.IP ?? "null"));
            cmd.Parameters.Add(new SqlParameter("@BrowserType", clientInfo.BrowserType ?? "null"));
            cmd.Parameters.Add(new SqlParameter("@BrowserVersion", clientInfo.BrowserVersion ?? "null"));

            cmd.ExecuteNonQuery();
        }
    }
}