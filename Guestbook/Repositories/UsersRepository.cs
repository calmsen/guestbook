﻿using Guestbook.Infrastructure;
using Guestbook.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Guestbook.Repositories
{
    /// <summary>
    /// Данный класс представляет методы для работы с пользователями
    /// </summary>
    public class UsersRepository
    {
        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private DefaultDatabase _db;
        /// <summary>
        /// Инициазирует экземпляр класса UsersRepository
        /// </summary>
        /// <param name="db">Контекст базы данных</param>
        public UsersRepository(DefaultDatabase db)
        {
            _db = db;
        }
        /// <summary>
        /// Создает пользователя
        /// </summary>
        /// <param name="user">Объект пользователя</param>
        public void CreateUser(User user)
        {           
            SqlCommand cmd = _db.CreateCommand(
                @"insert into Users (Name, Email, Homepage) values (@Name, @Email, @Homepage)
                select ID from Users where @@rowcount > 0 and ID = scope_identity()" 
            );

            cmd.Parameters.Add(new SqlParameter("@Name", user.Name));
            cmd.Parameters.Add(new SqlParameter("@Email", user.Email));
            cmd.Parameters.Add(new SqlParameter("@Homepage", user.Homepage ?? "null"));

            user.ID = (int)cmd.ExecuteScalar();
        }
        /// <summary>
        /// Получает пользователя по e-mail
        /// </summary>
        /// <param name="userEmail">E-mail пользователя</param>
        /// <returns></returns>
        public User GetUserByEmail(string userEmail)
        {
            User user = null;
            SqlDataReader rdr = null;
            try
            {
                SqlCommand cmd = _db.CreateCommand(
                    "select ID, Name, Email, Homepage from Users where Email = @Email"
                );

                cmd.Parameters.Add(new SqlParameter("@Email", userEmail));

                rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    user = new User { 
                        ID = Convert.ToInt32(rdr["ID"]),
                        Name = rdr["Name"].ToString(),
                        Email = rdr["Email"].ToString(),
                        Homepage = rdr["Homepage"].ToString()
                    };
                }
            }
            finally
            {
                if (rdr != null)
                    rdr.Close();
            } 
            return user;
        }
        /// <summary>
        /// Получает список пользователей 
        /// </summary>
        /// <param name="userIDs">Список идентификаторов пользователей</param>
        /// <returns></returns>
        public List<User> GetUsers(int[] userIDs)
        {
            List<User> users = new List<User>();

            if (userIDs == null || userIDs.Length == 0)
                return users;

            SqlDataReader rdr = null;
            try
            {
                // используем обычный string.Format вместо параметризованного запроса
                // так как нет строковых параметров, которые пришли от клиента
                SqlCommand cmd = _db.CreateCommand(string.Format(
                    "select ID, Name, Email, Homepage from Users where ID in ({0})",
                    string.Join(", ", userIDs)));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    users.Add(new User
                    {
                        ID = Convert.ToInt32(rdr["ID"]),
                        Name = rdr["Name"].ToString(),
                        Email = rdr["Email"].ToString(),
                        Homepage = rdr["Homepage"].ToString()
                    });
                }
            }
            finally
            {
                if (rdr != null)
                    rdr.Close();
            }
            return users;
        }
    }
}