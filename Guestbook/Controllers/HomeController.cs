﻿using Guestbook.Infrastructure;
using Guestbook.Models;
using Guestbook.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Guestbook.Controllers
{
    /// <summary>
    /// Контроллер для работы с гостевой книгой
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Контекс базы данных
        /// </summary>
        private DefaultDatabase _db;
        /// <summary>
        /// Репозиторий для работы с сообщениями гостевой книги
        /// </summary>
        private MessagesRepository _messagesReository;
        /// <summary>
        /// Репозиторий для работы с пользователями
        /// </summary>
        private UsersRepository _usersReository;
        /// <summary>
        /// Репозиторий для работы с информацией о клиенте
        /// </summary>
        private ClientInfosRepository _clientInfosRepository;
        /// <summary>
        /// Инициазирует экземпляр класса HomeController
        /// </summary>
        public HomeController()
        {
            // достанем ранне созданный контекст базы данных из HttpContext, и создадим репозитории
            _db = (DefaultDatabase)System.Web.HttpContext.Current.Items["DefaultDatabase"];
            _messagesReository = new MessagesRepository(_db);
            _usersReository = new UsersRepository(_db);
            _clientInfosRepository = new ClientInfosRepository(_db);
        }
        /// <summary>
        /// Возвращает главную страницу "Гостевая книга"
        /// </summary>
        /// <param name="page">Номер текущей страницы</param>
        /// <param name="sortType">Поле для сортировки сообщений в гостевой книги</param>
        /// <param name="sortDirection">Направление сортировки сообщений в гостевой книги</param>
        /// <returns></returns>
        public ActionResult Index(int page = 1, SortTypeEnum sortType = SortTypeEnum.CreatedDate, SortDirectionEnum sortDirection = SortDirectionEnum.Desc)
        {
            GuestbookViewModel model = GetGuestbook(page, sortType, sortDirection);
            return View(model);
        }
        /// <summary>
        /// Возвращает форму для добавления сообщения в гостевую книгу
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateMessage()
        {
            // получим url, для передачи его в ссылку "Отмена" в форме добавления сообщения 
            ViewBag.RedirectUrl = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "/";
            // получим пустую форму 
            return View(new Message { 
                User = new User()
            });
        }
        /// <summary>
        /// Добавляет сообщение в гостевую книгу. При создании сообщения не явным образом создается пользователь.
        /// </summary>
        /// <param name="message">Объект сообщения</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateMessage(Message message)
        {
            // если данные с формы пришли не валидные то вернем клиенту статус BadRequest
            if (!ModelState.IsValid) 
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // запускаем транзакцию 
            _db.BeginTransaction();     
            try
            {
                // получим пользователя в соответсвии указанного e-mail-а в форме
                // если такого пользователя нет в системе то создадим пользователя
                User user = _usersReository.GetUserByEmail(message.User.Email);
                if (user != null)
                {
                    message.UserID = user.ID;
                }
                else
                {
                    _usersReository.CreateUser(message.User);
                    message.UserID = message.User.ID;
                }
                // создадим сообщение
                _messagesReository.CreateMessage(message);
                // добавляем информацию об IP и браузере пользователя
                ClientInfo clientInfo = new ClientInfo
                {
                    MessageID = message.ID,
                    IP = GetClientIP(),
                    BrowserType = Request.Browser.Browser,
                    BrowserVersion = Request.Browser.Version
                };
                _clientInfosRepository.CreateClientInfo(clientInfo);
                // завершаем транзакцию фиксацией
                _db.CommitTransaction();
                // получим список сообщений гостевой книги для первой страницы
                return PartialView("Guestbook", GetGuestbook());
            }
            catch (Exception ex)
            {
                // завершаем транзакцию откатом
                _db.RollbackTransaction();
                // TODO: логировать ex
            }
            
            return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
        }
        /// <summary>
        /// Получает данные для гостевой книги
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="sortType">Поле для сортировки сообщений в гостевой книги</param>
        /// <param name="sortDirection">Направление сортировки сообщений в гостевой книги</param>
        /// <returns></returns>
        private GuestbookViewModel GetGuestbook(int page = 1, SortTypeEnum sortType = SortTypeEnum.CreatedDate, SortDirectionEnum sortDirection = SortDirectionEnum.Desc)
        {
            // получим объект пагинации
            Dictionary<string, object> routeParams = new Dictionary<string, object> 
            { 
                { "sortType", sortType }, 
                { "sortDirection", sortDirection }, 
                { "action", "Index" }, 
                { "controller", "Home" } 
            };
            int numberOfItems = _messagesReository.GetNumberOfItems();
            Pagination pagination = new Pagination(page, 25, numberOfItems, routeParams);
            // получим список сообщений для гостевой книги
            List<Message> messages = _messagesReository.GetMessages(sortType, sortDirection, pagination.Offset, pagination.PageItemsAmount);
            // получим пользователей и вложим их в сообщения
            int[] userIDs = messages.Select(x => x.UserID).Distinct().ToArray();
            List<User> users = _usersReository.GetUsers(userIDs);
            foreach (Message message in messages)
            {
                message.User = users.FirstOrDefault(x => x.ID == message.UserID);
            }
            // получим список ссылок, по которым будет сортировка
            List<HeaderLink> headerLinks = GetHeaderLinks(sortType, sortDirection);
            // получим данные для гостевой книги
            return new GuestbookViewModel
            {
                Title = "Гостевая книга",
                Messages = messages,
                Pagination = pagination,
                HeaderLinks = headerLinks
            };
        }
        /// <summary>
        /// Получает IP адрес клиента 
        /// </summary>
        /// <returns></returns>
        private string GetClientIP()
        {
            try
            {
                string ip = Request.Headers["X-Forwarded-For"];
                if (string.IsNullOrEmpty(ip) || "unknown".Equals(ip, StringComparison.InvariantCultureIgnoreCase))
                {
                    ip = Request.Headers["Proxy-Client-IP"];
                }
                if (string.IsNullOrEmpty(ip) || "unknown".Equals(ip, StringComparison.InvariantCultureIgnoreCase))
                {
                    ip = Request.Headers["WL-Proxy-Client-IP"];
                }
                if (string.IsNullOrEmpty(ip) || "unknown".Equals(ip, StringComparison.InvariantCultureIgnoreCase))
                {
                    ip = Request.Headers["HTTP_CLIENT_IP"];
                }
                if (string.IsNullOrEmpty(ip) || "unknown".Equals(ip, StringComparison.InvariantCultureIgnoreCase))
                {
                    ip = Request.Headers["HTTP_X_FORWARDED_FOR"];
                }
                if (string.IsNullOrEmpty(ip) || "unknown".Equals(ip, StringComparison.InvariantCultureIgnoreCase))
                {
                    ip = Request.ServerVariables["REMOTE_ADDR"];
                }
                if (ip != null)
                    ip = ip.Split(',')[0].Trim();
                IPAddress address = IPAddress.Parse(ip);
                return address.MapToIPv4().ToString();
            }
            catch (Exception ex)
            {
                // TODO: логировать ex
                return null;
            }
        }
        /// <summary>
        /// Получает список заголовков для сортировки
        /// </summary>
        /// <param name="sortType">Поле для сортировки сообщений</param>
        /// <param name="sortDirection">Направление сортировки</param>
        /// <returns></returns>
        private List<HeaderLink> GetHeaderLinks(SortTypeEnum sortType, SortDirectionEnum sortDirection)
        {
            Dictionary<string, object> routeParams = new Dictionary<string, object> 
            { 
                { "action", "Index" }, 
                { "controller", "Home" } 
            };
            List<HeaderLink> headerLinks = new List<HeaderLink>();
            foreach (SortTypeEnum currentSortType in Enum.GetValues(typeof(SortTypeEnum)))
            {
                // получим название ссылки
                string headerLinkName = null;
                switch (currentSortType)
                {
                    case SortTypeEnum.UserName: headerLinkName = "Имя";
                        break;
                    case SortTypeEnum.Email: headerLinkName = "E-mail";
                        break;
                    case SortTypeEnum.CreatedDate: headerLinkName = "Дата добавления";
                        break;
                }
                // установим активная ли ссылка. Если неактивная значение null
                SortDirectionEnum? activeDirection = null;
                if (currentSortType == sortType)
                    activeDirection = sortDirection;
                // получим направление сортировки для ссылки
                SortDirectionEnum currentSortDirection = SortDirectionEnum.Asc;
                if (currentSortType == sortType && sortDirection == SortDirectionEnum.Asc)
                    currentSortDirection = SortDirectionEnum.Desc;
                // получим параметры ссылки
                RouteValueDictionary routeValues = CreateRouteValues(currentSortType, currentSortDirection, routeParams);

                headerLinks.Add(new HeaderLink
                {
                    Name = headerLinkName,
                    ActiveDirection = activeDirection,
                    RouteValues = routeValues
                });
            }
            return headerLinks;
        }
        /// <summary>
        /// Создает словарь RouteValueDictionary из других словарей
        /// </summary>
        /// <param name="sortType">Поле для сортировки сообщений</param>
        /// <param name="sortDirection">Направление сортировки</param>
        /// <param name="routeParams">Другие параметры</param>
        /// <returns></returns>
        private RouteValueDictionary CreateRouteValues(SortTypeEnum sortType, SortDirectionEnum sortDirection, Dictionary<string, object> routeParams)
        {
            RouteValueDictionary routeValues = new RouteValueDictionary();
            if (routeParams != null && routeParams.Count > 0)
                routeParams.ToList().ForEach(x => { routeValues[x.Key] = x.Value; });
            routeValues.Add("sortType", sortType);
            routeValues.Add("sortDirection", sortDirection);
            return routeValues;
        }
    }
}